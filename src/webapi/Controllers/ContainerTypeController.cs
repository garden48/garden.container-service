﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using garden.container_service.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace garden.container_service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContainerTypeController : CRUDControllerBase<Models.ContainerTypeModel,Entities.ContainerType>
    {
        public ContainerTypeController(ILogger<ContainerTypeController> logger, ApplicationContext context,IMapper mapper)
            : base(logger,context,mapper)
        {
        }
    }
}
