﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using garden.container_service.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using garden.container_service;

namespace garden.container_service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContainerController : CRUDControllerBase<Models.ContainerModel,Entities.Container>
    {        public ContainerController(ILogger<ContainerController> logger, ApplicationContext context,IMapper mapper)
            : base(logger,context,mapper)
        {
        }
    }
}
