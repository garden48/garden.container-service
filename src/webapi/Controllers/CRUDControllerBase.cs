﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using garden.container_service.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace garden.container_service.Controllers
{
    public abstract class CRUDControllerBase<TModel, TEntity> : ControllerBase
        where TEntity : class
    {
        private readonly ILogger _logger;
        private readonly ApplicationContext _context;
        private readonly IMapper _mapper;

        protected CRUDControllerBase(ILogger logger, ApplicationContext context, IMapper mapper)
        {
            _logger = logger;
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public IEnumerable<TModel> Get()
        {
            return _context.GetDbSet<TEntity>()
                .Select(c => _mapper.Map<TModel>(c));
        }

        [HttpPost]
        public void Post(TModel model)
        {
            _context.GetDbSet<TEntity>()
                .Add(
                    _mapper.Map<TEntity>(model)
                );
            _context.SaveChanges();
        }
    }
}
