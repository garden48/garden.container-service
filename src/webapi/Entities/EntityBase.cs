using System;

namespace garden.container_service.Entities
{
    public abstract class EntityBase
    {
        public long Id {get;set;}
        public DateTime CreationDate {get;set;} = DateTime.Now;
        public DateTime UpdateDate {get;set;} = DateTime.Now;

    }
}
