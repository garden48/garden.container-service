using System;
using System.Collections.Generic;
using garden.container_service.Entities;
using Microsoft.EntityFrameworkCore;

namespace garden.container_service
{
public class ApplicationContext : DbContext
{
    public ApplicationContext(DbContextOptions options)
            : base(options)
    {
    }
    public DbSet<Container> Containers { get; set; }
    public DbSet<ContainerType> ContainerTypes { get; set; }

    public DbSet<T> GetDbSet<T>() where T : class
    {
        //TODO To improve
        if (typeof(T) == typeof(Entities.Container)) return Containers as DbSet<T>;
        if (typeof(T) == typeof(Entities.ContainerType)) return ContainerTypes as DbSet<T>;
        throw new ArgumentException($"Unknown Type : {typeof(T)}");
    }
}
}
