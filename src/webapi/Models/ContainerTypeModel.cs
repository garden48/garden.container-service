using System;

namespace garden.container_service.Models
{
    public class ContainerTypeModel : ModelBase
    {
        public string ShortDescription {get;set;}
    }
}
