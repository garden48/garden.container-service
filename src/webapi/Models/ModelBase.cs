using System;

namespace garden.container_service.Models
{
    public abstract class ModelBase
    {
        public long Id {get;set;}
        public DateTime CreationDate {get;set;} = DateTime.Now;

    }
}
