// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this

using AutoMapper;

namespace garden.container_service.AutoMapperProfiles
{
    public class EntitiesToModels : Profile
    {
        public EntitiesToModels()
        {
            CreateMap<Entities.Container, Models.ContainerModel>()
                .ReverseMap();
            CreateMap<Entities.ContainerType, Models.ContainerTypeModel>()
                .ReverseMap();
        }
    }
}
